package com.example.exam.Controller;

import com.example.exam.Impl.StudentService;
import com.example.exam.DTO.StudentDTO;
import com.example.exam.entity.StudentEntity;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/student")
public class StudentController {
    private final ModelMapper mapper;
    private final StudentService studentService;

    @PostMapping("/new")
    public Boolean saveStudent(@RequestBody StudentEntity entity) {
        try {
            studentService.addStudent(entity);
            return true;
        }
        catch (Exception e){
            return false;
        }
    }

    @PostMapping("/delete")
    public Boolean deleteStudent(@RequestBody StudentEntity entity){
        try {
            studentService.deleteStudent(entity);
            return true;
        }
        catch (Exception e){
            return false;
        }

    }
    @PostMapping("/delete/{id}")
    public Boolean deleteStudent(@PathVariable("id") Integer id){
        try{
            studentService.deleteStudent(id);
            return true;
        }
        catch (Exception e){return false;}
    }

    @GetMapping("/get/{id}")
    public StudentDTO getStudent(@PathVariable("id") Integer id){
        return mapper.map(studentService.getStudentById(id), StudentDTO.class);
    }

    @GetMapping("/getall")
    public List<StudentDTO> getAllStudents(){
        List<StudentDTO> dtos = new ArrayList<>();
        List<StudentEntity> entities = studentService.getStudents();
        for (StudentEntity e:entities) {
            dtos.add(mapper.map(e, StudentDTO.class));
        }
        return dtos;
    }
}
