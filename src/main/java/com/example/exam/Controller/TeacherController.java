package com.example.exam.Controller;


import com.example.exam.DTO.StudentDTO;
import com.example.exam.DTO.TeacherDTO;

import com.example.exam.Impl.TeacherService;
import com.example.exam.entity.StudentEntity;
import com.example.exam.entity.TeacherEntity;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping("teacher/")
public class TeacherController {
    private final ModelMapper mapper;
    private final TeacherService teacherService;

    @PostMapping("/new")
    public Boolean saveTeacher(@RequestBody TeacherEntity entity) {
        try {
            teacherService.add(entity);
            return true;
        }
        catch (Exception e){
            return false;
        }
    }

    }
    @PostMapping("/delete/{id}")
    public Boolean deleteStudent(@PathVariable("id") Integer id){
        try{
            teacherService.deleteTeacher(id);
            return true;
        }
        catch (Exception e){return false;}
    }

    @GetMapping("/get/{id}")
    public StudentDTO getStudent(@PathVariable("id") Integer id){
        return mapper.map(teacherService.getTeacher(id), StudentDTO.class);
    }

    @GetMapping("/getall")
    public List<TeacherDTO> getAllStudents(){
        List<TeacherDTO> dtos = new ArrayList<>();
        List<TeacherEntity> entities = teacherService.getTeachers();
        for (TeacherEntity e:entities) {
            dtos.add(mapper.map(e, TeacherDTO.class));
        }
        return dtos;
    }
}
