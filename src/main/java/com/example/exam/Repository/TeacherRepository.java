package com.example.exam.Repository;

import com.example.exam.entity.TeacherEntity;
import org.springframework.data.repository.CrudRepository;

public interface TeacherRepository extends CrudRepository<TeacherEntity,Integer> {
}
