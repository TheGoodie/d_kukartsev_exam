package com.example.exam.Repository;

import com.example.exam.DTO.StudentDTO;

import java.util.List;

import com.example.exam.entity.StudentEntity;
import org.springframework.data.repository.CrudRepository;
//работает с БД
public interface StudentRepository extends CrudRepository<StudentEntity,Integer> {
}

