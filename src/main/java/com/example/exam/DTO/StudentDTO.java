package com.example.exam.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentDTO implements Serializable {
    Integer id;
    String name;
    Integer form;
    String speciality;
}
