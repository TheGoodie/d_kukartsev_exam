package com.example.exam.Impl;

import com.example.exam.Repository.TeacherRepository;
import com.example.exam.entity.StudentEntity;
import com.example.exam.entity.TeacherEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TeacherService {

    @Autowired
    private TeacherRepository teacherRepository;

    public TeacherEntity add(TeacherEntity teacherEntity){
        return teacherRepository.save(teacherEntity);
    }

    public TeacherEntity getTeacher(int id) {
        Optional<TeacherEntity> teacher = teacherRepository.findById(id);
        return teacher.get();
    }

    public int deleteTeacher(int id){
        teacherRepository.deleteById(id);
        return id;
    }

    public List<TeacherEntity> getTeachers(){
        ArrayList<TeacherEntity> listTeachers = new ArrayList<>();
        Iterable<TeacherEntity> iterable = teacherRepository.findAll();
        iterable.forEach(listTeachers::add);
        return listTeachers;
    }
}
