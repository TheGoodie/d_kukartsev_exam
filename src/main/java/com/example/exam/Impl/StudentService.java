package com.example.exam.Impl;

import com.example.exam.Repository.StudentRepository;
import com.example.exam.DTO.StudentDTO;
import com.example.exam.Repository.TeacherRepository;
import com.example.exam.entity.StudentEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class StudentService {

    @Autowired
    private StudentRepository studentRepository;

    public StudentEntity addStudent(StudentEntity entity){
        return studentRepository.save(entity);
    }

    public StudentEntity getStudentById(int id)  {
        Optional<StudentEntity> student = studentRepository.findById(id);
        return student.get();
    }

    public void deleteStudent(int id) {
        studentRepository.deleteById(id);
    }
    public void deleteStudent(StudentEntity entity){
        studentRepository.delete(entity);
    }

    public List<StudentEntity> getStudents() {
        ArrayList<StudentEntity> listStudents = new ArrayList<>();
        Iterable<StudentEntity> iterable = studentRepository.findAll();
        iterable.forEach(listStudents::add);
        return listStudents;
    }
}
